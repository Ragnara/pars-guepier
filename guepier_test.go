package main

func Example() {
	classifyString("")
	classifyString("a")
	classifyString("ac")
	classifyString("aaac")
	classifyString("abc")
	classifyString("abab")
	classifyString("ababc")
	classifyString("abbc")
	classifyString("bc")
	classifyString("c")

	//Output:
	//'' does not correspond to the grammar
	//'a' does not correspond to the grammar
	//'ac' corresponds to the grammar
	//'aaac' corresponds to the grammar
	//'abc' corresponds to the grammar
	//'abab' does not correspond to the grammar
	//'ababc' corresponds to the grammar
	//'abbc' corresponds to the grammar
	//'bc' corresponds to the grammar
	//'c' corresponds to the grammar
}
