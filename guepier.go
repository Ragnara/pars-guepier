//Implementation of the grammar from https://www.reddit.com/r/programming/comments/acrb49/you_could_have_invented_parser_combinators/ with bitbucket.org/ragnara/pars
package main

import (
	"bitbucket.org/ragnara/pars"
	"bufio"
	"fmt"
	"os"
)

func main() {
	fmt.Println("Implementation of the grammar from https://www.reddit.com/r/programming/comments/acrb49/you_could_have_invented_parser_combinators/ with pars")
	fmt.Println("S ⩴ a S | A")
	fmt.Println("A ⩴ b S | c")
	fmt.Println("Close StdIn to quit")

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line := scanner.Text()
		classifyString(line)
	}
}

func classifyString(s string) {
	if parseString(s) {
		fmt.Printf("'%v' corresponds to the grammar\n", s)
	} else {
		fmt.Printf("'%v' does not correspond to the grammar\n", s)
	}
}

func parseString(s string) bool {
	_, err := pars.ParseString(s, pars.DiscardRight(newSParser(), pars.EOF))
	return err == nil
}

func newSParser() pars.Parser {
	return pars.Or(pars.SplicingSeq(pars.Char('a'), pars.Recursive(newSParser)), pars.Recursive(newAParser))
}

func newAParser() pars.Parser {
	return pars.Or(pars.SplicingSeq(pars.Char('b'), pars.Recursive(newSParser)), pars.Char('c'))
}
